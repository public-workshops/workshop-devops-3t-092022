package tn.training3t.hello.spring.boot;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomerServiceImplTests {

	@Autowired
	CustomerService customerService;

	@Test
	public void testAddCustomer() {
		List<Customer> customers = customerService.readAll();
		int expected = customers.size();
		Customer c = new Customer();
		c.setId(1L);
		c.setFullName("Angelina JOOLIE");
		c.setAge(40);
		c.setIsProfessional(true);
		Customer savedCustomer = customerService.create(c);
		assertEquals(expected + 1, customerService.readAll().size());
		assertNotNull(savedCustomer.getFullName());
	}
}
