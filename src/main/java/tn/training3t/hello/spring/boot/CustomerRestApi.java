package tn.training3t.hello.spring.boot;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/clients")
public class CustomerRestApi {

	@Autowired
	private CustomerService customerService;
	
	@PostMapping("/")
	public Customer save(@RequestBody CustomerDto c)
	{
		return customerService.create(CustomerDto.fromCustomerDto(c));
	}
	
	@GetMapping("/{id}")
	public CustomerDto getById(@PathVariable Long id)
	{
		return CustomerDto.fromCustomer(customerService.readById(id));
	}
	
	@GetMapping("/")
	public List<CustomerDto> getAll()
	{
		return CustomerDto.fromCustomers(customerService.readAll());
	}
	
	@PutMapping("/")
	public Customer update(@RequestBody CustomerDto c)
	{
		if (customerService.readById(c.getId())!=null)
		{
			return customerService.update(CustomerDto.fromCustomerDto(c));
		}
		return null;
	}
	
	@DeleteMapping("/{id}")
	public void deleteById(@PathVariable Long id)
	{
		customerService.delete(id);
	}
	
	/** V1 **/
	
	/*
	@PostMapping("/")
	public Customer save(@RequestBody Customer c)
	{
		return customerService.create(c);
	}
	
	@GetMapping("/{id}")
	public Customer getById(@PathVariable Long id)
	{
		return customerService.readById(id);
	}
	
	@GetMapping("/")
	public List<Customer> getAll()
	{
		return customerService.readAll();
	}
	
	@PutMapping("/")
	public Customer update(@RequestBody Customer c)
	{
		if (customerService.readById(c.getId())!=null)
		{
			return customerService.create(c);
		}
		return null;
	}
	
	@DeleteMapping("/{id}")
	public void deleteById(@PathVariable Long id)
	{
		customerService.delete(id);
	}
	*/
}
