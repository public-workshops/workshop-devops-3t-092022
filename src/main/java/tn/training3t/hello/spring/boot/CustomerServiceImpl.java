package tn.training3t.hello.spring.boot;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	private CustomerRepository customerRepository;
	
	@Override
	public Customer create(Customer c) {
		return customerRepository.save(c);
	}

	@Override
	public Customer update(Customer c) {
		return customerRepository.save(c);
	}

	@Override
	public Customer readById(Long id) {
		Optional<Customer> customerOptional = customerRepository.findById(id);
		return customerOptional.isPresent()? customerOptional.get() : null;
	}

	@Override
	public List<Customer> readAll() {
		return customerRepository.findAll();
	}

	@Override
	public void delete(Customer c) {
		customerRepository.delete(c);
	}

	@Override
	public void delete(Long id) {
		customerRepository.deleteById(id);
	}

}
