package tn.training3t.hello.spring.boot;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloApi {

	@GetMapping("/hello/{name}")
	public String sayHello(@PathVariable String name)
	{
		return "Hello "+name;
	}
	
}
