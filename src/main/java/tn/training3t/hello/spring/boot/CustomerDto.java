package tn.training3t.hello.spring.boot;

import java.util.ArrayList;
import java.util.List;

public class CustomerDto {

	private Long id;
	
	private String fullName;
	
	private Integer age;
	
	private Boolean isProfessional;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public Boolean getIsProfessional() {
		return isProfessional;
	}

	public void setIsProfessional(Boolean isProfessional) {
		this.isProfessional = isProfessional;
	}
	
	
	public static CustomerDto fromCustomer(Customer c)
	{
		CustomerDto cd = new CustomerDto();
		cd.setId(c.getId());
		cd.setAge(c.getAge());
		cd.setFullName(c.getFullName());
		cd.setIsProfessional(c.getIsProfessional());
		return cd;
	}
	
	public static Customer fromCustomerDto(CustomerDto cd)
	{
		Customer c = new Customer();
		c.setId(cd.getId());
		c.setAge(cd.getAge());
		c.setFullName(cd.getFullName());
		c.setIsProfessional(cd.getIsProfessional());
		return c;
	}
	
	public static List<CustomerDto> fromCustomers(List<Customer> l)
	{
		List<CustomerDto> cdl = new ArrayList<CustomerDto>();
		for(Customer c : l)
		{
			cdl.add(fromCustomer(c));
		}
		return cdl;
	}
	
}
